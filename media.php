<?php 
session_start();

if (empty($_SESSION['username']) AND empty($_SESSION['password'])){
  echo "<div id=\"login\"><h1 class=\"fail\">Untuk mengakses halaman ini, Anda harus login dulu.</h1><p class=\"fail\"><a href=\"index.php\">LOGIN</a></p></div>";  
}

else{
?>
<?php 

    $servername = "localhost";
    $username   = "root";
    $password   = "";
    $database   = "css_biodata";

    $koneksi = new mysqli($servername,$username,$password,$database);

    if ($koneksi->connect_error) {
        die("Connction failed :".$koneksi->connect_error);
    }

    if (isset($_POST['tbcari'])) {
        $cari = $_POST['cari'];
        $query = mysqli_query($koneksi,"SELECT * FROM tambah WHERE nama like '%$cari%'");
    }else{
         $query   = mysqli_query($koneksi,"SELECT * FROM tambah");
         //$hasil   = $koneksi->query($query);
    }

   
?>


 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>Halaman Utama</title>
    <style type="text/css">
    a:link, a:visited {
                background-color: white;
                color: black;
                border: 2px solid #f6c4a6;
                padding: 5px 10px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                border-radius: 10px;
              }

              a:hover, a:active {
                background-color: #82b1ff;
                color: white;
              }
              table {
              border-collapse: collapse;
              width: 100%;
            }

            tbody thead, td {
              text-align: left;
              padding: 8px;
            }

           tbody tr:nth-child(even){background-color: beige;}

            thead {
              background-color: #03254c;
              color: white;
              }
            .image-wrapper {
                width: 100px;
            }
            .image-wrapper img {
                width: 100%;
            }
            .image-wrapper span {
                font-family: sans-serif;
                font-size: 10px;
                color: #ccc;
            }
            .image-wrapper-detail {
                width: 300px;
            }
            .image-wrapper-detail img {
                width: 100%;
            }
  </style>
 </head>
 <body>
  <div id="wrapper">

      <div id="header">
        <h2>Selamat Datang Anggota</h2>
      </div>
        <div>
        <table>
        <tr>
        <td colspan="2"><a href="tambah.php">Tambah Data</a>||
          <a href="logout.php" onClick="return confirm ('Yakin? Mau Keluar!')">Logout</a>
        </tr>
        </table>
        <form action="" method="POST">
            <table>
            <td colspan="2">
              <input type="text" name="cari" placeholder="cari data anda disni" size="45px" autofocus autocomplete="off" 
              value="<?php if(isset($_POST['cari'])){echo $_POST['cari'];} ?>"> ||
              <button type="submit" name="tbcari">cari</button>
            </td>
          </tr>
          </table> 
        </form>
        <br>
          <table>
            <thead>
              <tr>
                <td>Id_Tambah</td>
                <td>NIM</td>
                <td>Nama Lengkap</td>
                <td>Alamat</td>
                <td>Email</td>
                <td>Foto</td>
                <td>Aksi</td>
              </tr>
            </thead>
            <tbody>
              <?php 

                         if ($query->num_rows > 0) {
                            while ($rows = $query->fetch_assoc()) {
                            //echo $rows['nama'];
                                echo '
                                    <tr>
                                    <td>'.$rows['id_tambah'].'</td>
                                    <td>'.$rows['nim'].'</td>
                                    <td>'.$rows['nama'].'</td>
                                    <td>'.$rows['alamat'].'</td>
                                    <td>'.$rows['email'].'</td>
                                    <td>
                                        <div class="image-wrapper">
                                            <img src="gambar/'.$rows['foto'].'">
                                        </div>
                                    </td>
                                    <td>
                                        <a href="biodata.php?id_tambah='.$rows['id_tambah'].'">Publish</a> | 
                                        <a href="edit.php?id_tambah='.$rows['id_tambah'].'">Edit</a> | 
                                        <a href="delete.php?id_tambah='.$rows['id_tambah'].'&foto='.$rows['foto'].'" onClick="return confirm(\'Apakah Anda Yakin Ingin Menghapus Berita Ini?\')">Delete</a>
                                    </td>
                                </tr>
                                ';

                             }
                        } 

                     ?>
            
                </tbody>
            </tbody>
          </table>
        </table>
      </div>
  
      <div id="footer">
         <p style="color:white;">Copyright@hadiahmadsobari2023</p>
      </div>
    </div> 
 </body>
 </html>
 <?php } ?>