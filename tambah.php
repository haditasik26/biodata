<?php 
session_start();

if (empty($_SESSION['username']) AND empty($_SESSION['password'])){
  echo "<div id=\"login\"><h1 class=\"fail\">Untuk mengakses halaman ini, Anda harus login dulu.</h1><p class=\"fail\"><a href=\"index.php\">LOGIN</a></p></div>";  
}

else{
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Halaman Tambah Data Baru</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<div style="background-color:#ebebeb;color:black;padding:20px;">

	<form action="aksi_tambah.php" method="POST" name="form1" enctype="multipart/form-data">

		<table>
			<center><h2>DETAIL DATA PRIBADI</h2></center>
			<tr>
				<td><label for="nim">NIM</label></td>
				<td><input type="text" name="nim" id="nim" size="35px"></td>
			</tr>
			<tr>
				<td><label for="nama">Nama Lengkap</label></td>
				<td><input type="text" name="nama" id="nama" size="35px"></td>
			</tr>
				<tr>
				<td><label for="alamat">Alamat</label></td>
				<td><input type="text" name="alamat" id="alamat" size="35px"></td>
			</tr>
				<tr>
				<td><label for="kelahiran">Tempat,tanggal lahir</label></td>
				<td><input type="text" name="kelahiran" id="kelahiran" size="35px"></td>
			</tr>
				<tr>
				<td><label for="kelamin">Jenis Kelamin</label></td>
				<td><input type="text" name="kelamin" id="kelamin" size="35px"></td>
			</tr>
				<tr>
				<td><label for="status">Status</label></td>
				<td><input type="text" name="status" id="nama" size="35px"></td>
			</tr>
			<tr>
				<td><label for="negara">Negara</label></td>
				<td><input type="text" name="negara" id="negara" size="35px"></td>
			</tr>
		</table>
		<hr style="color:blue;">
		<table>
			<center><h2>Sosial Media</h2></center>
			<tr>
				<td><label for="fb">Facebook</label></td>
				<td><input type="text" name="fb" id="fb" size="50px" placeholder="Masukan Url Facebok"></td>
			</tr>
			<tr>
				<td><label for="email">Email</label></td>
				<td><input type="text" name="email" id="email" size="50px" placeholder="Masukan Url Email"></td>
			</tr>
			<tr>
				<td><label for="ig">Instagram</label></td>
				<td><input type="text" name="ig" id="ig" size="50px" placeholder="Masukan Url Instagram"></td>
			</tr>
			<tr>
				<td><label for="wa">WhatssApp</label></td>
				<td><input type="text" name="wa" id="wa" size="50px" placeholder="Masukan Url WhatssApp"></td>
			</tr>
		</table>
		<hr style="color:blue;">
		<center><h2>PENGALAMAN TERKAIT</h2></center>
		<table>
			<tr>
				<td><label for="pengalaman1">Pengalaman 1</label></td>
				<td><textarea name="pengalaman1" id="pengalaman1" cols="75px" rows="10px"></textarea></td>
			</tr>
			<tr>
				<td><label for="pengalaman2">Pengalaman 2</label></td>
				<td><textarea name="pengalaman2" id="pengalaman2" cols="75px" rows="10px"></textarea></td>
			</tr>
		</table><hr style="color:blue;">
		<table>
			<center><h2>PENDIDIKAN DAN KETERAMPILAN</h2></center>
		<tr>
				<td><label for="pendidikan">Pendidikan</label></td>
				<td><textarea name="pendidikan" id="pendidikan" cols="75px" rows="10px"></textarea></td>
		</tr>
		<tr>
				<td><label for="keterampilan">keterampilan</label></td>
				<td><textarea name="keterampilan" id="keterampilan" cols="75px" rows="10px"></textarea></td>
			</tr>
		</table><hr style="color: blue;">
		<table>
			<tr>
				<td><label for="tentang">Tentang</label></td>
				<td><textarea name="tentang" id="tentang" cols="75px" rows="10px"></textarea></td>
			</tr>
			<tr> 
        <td>Upload Foto</td>
        <td><input type="file" name="fotobio" id="fileToUpload"></td>
      </tr>
		</table><hr style="color:blue;">
		<center>
			<table>
				<tr>
					<td colspan="3">
						<button type="submit" name="submit">simpan</button> ||
						<button type="reset" name="reset">reset</button> || <a href="media.php"> kembali</a>
					</td>
				</tr>
			</table><hr style="color:blue;">
		</center>
	</form>
</div>

</body>
</html>
<?php } ?>